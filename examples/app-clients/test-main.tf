provider "aws" {
  region = "eu-west-1"
}

module "user_pool" {
  source = "../../"

  project     = "test"
  environment = "app-clients-example"

  user_pool_name = "app-clients-example-user-pool"

  user_pool_alias_attributes         = ["email", "phone_number"]
  user_pool_auto_verified_attributes = ["email"]

  user_pool_custom_attributes = {
    foo = {
      name                = "foo"
      attribute_data_type = "String"
      string_attribute_constraints = {
        min_length = 1
        max_length = 256
      }
    }
    bar = {
      name                = "bar"
      attribute_data_type = "Number"
      number_attribute_constraints = {
        min_value = 1
        max_value = 100
      }
    }
  }

  user_pool_password_min_length    = 30
  user_pool_password_req_lowercase = false
  user_pool_password_req_uppercase = false
  user_pool_password_req_numbers   = false
  user_pool_password_req_symbols   = false

  remember_device = true

  user_pool_groups = {
    testgroup = "this is a test group"
  }

  user_pool_clients = {
    client1 = {
      name                 = "test-client-1"
      generate_secret      = true
      allow_oauth_flows    = true
      callback_urls        = ["https://google.com/foo/bar"]
      callback_domains     = ["microsoft.com"]
      logout_urls          = ["https://bing.com"]
      allowed_oauth_flows  = ["code"]
      allowed_oauth_scopes = ["openid", "email"]
      explicit_auth_flows  = ["ADMIN_NO_SRP_AUTH"]
      identity_providers   = ["COGNITO"]
      write_attributes     = []
    }
    client1 = {
      name                 = "test-client-2"
      generate_secret      = false
      allow_oauth_flows    = false
      callback_urls        = ["https://google.comfoo/bar"]
      callback_domains     = ["microsoft.com"]
      logout_urls          = ["https://bing.com"]
      allowed_oauth_flows  = []
      allowed_oauth_scopes = []
      explicit_auth_flows  = []
      identity_providers   = ["COGNITO"]
      write_attributes     = []
    }
  }
}
